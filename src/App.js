import logo from "./logo.svg";
import "./App.css";
import Home from "./components/Home";
import Complete from "./components/Complete";
import Todo from "./components/Todo";
import { Routes, Route, Link } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Calender from "./components/Calender";
function App() {
  return (
    <div className="App">
      <Header></Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="complete" element={<Complete />} />
        <Route path="todo" element={<Todo />} />
        <Route path="calender" element={<Calender />} />
      </Routes>
      <Footer></Footer>
    </div>
  );
}

export default App;
