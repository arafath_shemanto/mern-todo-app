import React, { useState } from "react";
import Table from "react-bootstrap/Table";
import { useQuery } from "react-query";
import Home_tableRow from "./todoSingleRow/Home_tableRow";
const Home = () => {
  const todoSubmit = (e) => {
    e.preventDefault();
    const todo_info = e.target.todoInfo.value;
    const inputval = { todo_info };
    const url = "https://morning-everglades-59869.herokuapp.com/todos";
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(inputval),
    })
      .then((res) => res.json())
      .then((result) => console.log(result));
    e.target.reset();
  };

  const {
    isLoading,
    error,
    data: todos,
    isFetching,
    refetch,
  } = useQuery("todos", () =>
    fetch("https://morning-everglades-59869.herokuapp.com/todos", {
      method: "GET",
    }).then((res) => res.json())
  );
  if (isLoading) {
    return <p>loading</p>;
  }
  return (
    <div className="section_padding">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 mb-4">
            <form onSubmit={todoSubmit}>
              <div className="row">
                <div className="col-12">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="add your task"
                    name="todoInfo"
                  />
                </div>
              </div>
            </form>
          </div>
          <div className="col-12">
            <Table striped bordered hover responsive>
              <thead>
                <tr>
                  <th>#</th>
                  <th>todo Name</th>
                </tr>
              </thead>
              <tbody>
                {todos.map((item) => (
                  <Home_tableRow
                    key={item._id}
                    item={item}
                    refetch={refetch}
                  ></Home_tableRow>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
