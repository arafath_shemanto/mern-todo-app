import React from "react";
import { Button } from "react-bootstrap";
import { ImCheckboxUnchecked, ImCheckboxChecked } from "react-icons/im";
const TodoEditRow = ({
  item,
  setWewTodoInfo,
  refetch,
  editHandeler,
  showButton,
  EditButtonSHow,
  index,
}) => {
  console.log(item._id);
  const { role } = item;
  const completeTask = (id) => {
    console.log("lollll", id);
    fetch(
      `https://morning-everglades-59869.herokuapp.com/todos/complete_task/${id}`,
      {
        method: "PUT",
        headers: {
          "content-type": "application/json",
        },
      }
    )
      .then((res) => res.json())
      .then((data) => console.log(data));
  };
  console.log(item);
  refetch();
  return (
    <tr>
      <td>
        <Button
          className="text-nowrap custom_cehck_icon"
          onClick={(e) => completeTask(item._id)}
        >
          {role ? (
            <ImCheckboxChecked></ImCheckboxChecked>
          ) : (
            <ImCheckboxUnchecked></ImCheckboxUnchecked>
          )}
        </Button>
      </td>
      <td>{item.todo_info}</td>
      <td>
        {EditButtonSHow && (
          <input
            placeholder="type your updated value"
            type="text"
            onChange={(e) => setWewTodoInfo(e.target.value)}
          />
        )}
      </td>
      <td>
        <div className="d-flex gap-2">
          {EditButtonSHow && (
            <Button
              className="btn btn-success me-2 text-nowrap"
              onClick={(e) => editHandeler(item._id)}
            >
              click to Update
            </Button>
          )}

          <Button className="text-nowrap" onClick={(e) => showButton()}>
            Start Edit
          </Button>
        </div>
      </td>
    </tr>
  );
};

export default TodoEditRow;
