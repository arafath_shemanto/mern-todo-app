import React from "react";

const Home_tableRow = ({ item, refetch }) => {
  refetch();
  return (
    <tr>
      <td>#</td>
      <td>{item.todo_info}</td>
    </tr>
  );
};

export default Home_tableRow;
