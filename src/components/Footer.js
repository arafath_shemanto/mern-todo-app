import React from "react";
// import payments from "../../assets/img/payments.png";
const Footer = () => {
  return (
    <footer className="home_footer">
      <div className="main_footer_wrap">
        <div className="container">
          <div className="row">
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="footer_widget">
                <ul className="footer_links">
                  <li>
                    <a href="my_account.php">MY ACCOUNT</a>
                  </li>
                  <li>
                    <a href="my_order.php">ORDER STATUS</a>
                  </li>
                  <li>
                    <a href="my_order.php">SHIPPING</a>
                  </li>
                  <li>
                    <a href="billing_address.php">BILLING</a>
                  </li>
                  <li>
                    <a href="my_order.php">RETURNS & EXCHANGES</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="footer_widget">
                <ul className="footer_links">
                  <li>
                    <a href="contact.php">CUSTOMER SERVICE</a>
                  </li>
                  <li>
                    <a href="contact.php">CONTACT US</a>
                  </li>
                  <li>
                    <a href="contact.php">ACCESSIBILITY</a>
                  </li>
                  <li>
                    <a href="my_order.php">INTERNATIONAL SHIPMENTS</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-3 col-xl-3 col-md-6">
              <div className="footer_widget">
                <h4 className="large_address">
                  <span>0045</span> 500 123 994
                </h4>
                <div className="footer_opening_time">
                  <p>Monday - Friday: 9:00 - 20:00</p>
                  <p>Saturday: 11:00 - 15:00</p>
                </div>
                <div className="help_text">
                  <p>
                    NEED HELP? <a href="#">CONTACT@EXAMPLE.COM</a>
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-3  col-md-6">
              <div className="footer_widget">
                <p className="address_text">
                  PO BOX 16122 COLLINS STREET VICTORIA 8007 AUSTRALIA
                </p>
                <div className="social__Links">
                  <a href="#">
                    <i className="fab fa-twitter"></i>
                  </a>
                  <a href="#">
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                  <a href="#">
                    <i className="fab fa-instagram"></i>
                  </a>
                  <a href="#">
                    <i className="fab fa-facebook"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright_area">
        <div className="container">
          <div className="footer_border"></div>
          <div className="row">
            <div className="col-md-12">
              <div className="copy_right_text text-center">
                <p>
                  © 2022 <a href="#">Todo app</a>. All rights reserved. Made By{" "}
                  <a href="#">Arafath</a>.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
