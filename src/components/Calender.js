import React from "react";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
const Calender = () => {
  return (
    <div className="section_padding">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <FullCalendar
              plugins={[dayGridPlugin]}
              initialView="dayGridMonth"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Calender;
