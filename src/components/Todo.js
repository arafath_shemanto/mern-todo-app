import React, { useEffect, useState } from "react";

import Table from "react-bootstrap/Table";
import TodoEditRow from "./todoSingleRow/TodoEditRow";
import { Button } from "react-bootstrap";
import { useQuery } from "react-query";
const Todo = () => {
  //   const [todolist, setTodoList] = useState([]);
  const [newTodoInfo, setWewTodoInfo] = useState("");
  const [EditButtonSHow, setEditButtonSHow] = useState(false);
  console.log(newTodoInfo);
  const {
    isLoading,
    error,
    data: todolist,
    isFetching,
    refetch,
  } = useQuery("todos", () =>
    fetch("https://morning-everglades-59869.herokuapp.com/todos", {
      method: "GET",
    }).then((res) => res.json())
  );
  if (isLoading) {
    return <p>loading</p>;
  }
  const editHandeler = (id) => {
    const todo_info = newTodoInfo;
    const inputval = { todo_info };
    console.log("here is ", id);
    const url = `https://morning-everglades-59869.herokuapp.com/todos/${id}`;
    fetch(url, {
      method: "PUT",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(inputval),
    })
      .then((res) => res.json())
      .then((result) => console.log(result));
  };
  const showButton = () => {
    setEditButtonSHow(!EditButtonSHow);
    console.log(EditButtonSHow);
  };
  return (
    <div>
      <div className="section_padding">
        <div className="container">
          <div className="row">
            <div className="col-xl-8"></div>
            <div className="col-12">
              <Table striped bordered hover responsive>
                <thead>
                  <tr>
                    <th># </th>
                    <th>Todo Name</th>
                    <th>Updated field</th>
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>
                  {todolist.map((item, index) => (
                    <TodoEditRow
                      key={item._id}
                      item={item}
                      index={index}
                      refetch={refetch}
                      editHandeler={editHandeler}
                      setWewTodoInfo={setWewTodoInfo}
                      showButton={showButton}
                      EditButtonSHow={EditButtonSHow}
                    ></TodoEditRow>
                  ))}
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Todo;
