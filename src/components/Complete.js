import React from "react";
import { useQuery } from "react-query";
import Table from "react-bootstrap/Table";
const Complete = () => {
  const {
    isLoading,
    error,
    data: todolist,
    isFetching,
    refetch,
  } = useQuery("todos", () =>
    fetch("https://morning-everglades-59869.herokuapp.com/todos", {
      method: "GET",
    }).then((res) => res.json())
  );
  console.log(todolist);
  if (isLoading) {
    return <p>loading</p>;
  }
  const completeOnly = todolist.filter((item) => item.role === "complete");
  console.log(completeOnly);
  return (
    <div className="section_padding">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-xl-8 text-center mb-3">
            <h3 className="text-capitalize">all completed task here</h3>
          </div>
          <div className="col-12">
            <Table striped bordered hover responsive>
              <thead>
                <tr>
                  <th>Completed list todo Name</th>
                </tr>
              </thead>
              <tbody>
                {completeOnly?.map((item) => (
                  <tr key={item._id}>
                    <td>{item.todo_info}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Complete;
